package com.agiletestingalliance;

public class MinMax {

    public int function1(int variableA, int variableB) {
        if (variableB > variableA){
            return variableB;
        }
        else {
            return variableA;
        }
    }

}
